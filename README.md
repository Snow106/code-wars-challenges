# Code Wars Challenges

My Code Wars solutions.


<h2>6 Kyu:</h2>

<h3>Playing with digits</h3>

<h4>Instructions:</h4>
Some numbers have funny properties. For example:

>89 --> 8¹ + 9² = 89 * 1

>695 --> 6² + 9³ + 5⁴= 1390 = 695 * 2

>46288 --> 4³ + 6⁴+ 2⁵ + 8⁶ + 8⁷ = 2360688 = 46288 * 51

Given a positive integer n written as abcd... (a, b, c, d... being digits) and a positive integer p

>we want to find a positive integer k, if it exists, such as the sum of the digits of n taken to the successive powers of p is equal to k * n.

In other words:

>Is there an integer k such as : (a ^ p + b ^ (p+1) + c ^(p+2) + d ^ (p+3) + ...) = n * k

If it is the case we will return k, if not return -1.

Note: n and p will always be given as strictly positive integers.
```
digPow(89, 1) should return 1 since 8¹ + 9² = 89 = 89 * 1
digPow(92, 1) should return -1 since there is no k such as 9¹ + 2² equals 92 * k
digPow(695, 2) should return 2 since 6² + 9³ + 5⁴= 1390 = 695 * 2
digPow(46288, 3) should return 51 since 4³ + 6⁴+ 2⁵ + 8⁶ + 8⁷ = 2360688 = 46288 * 51
```

<h4>Solution</h4>

```cpp
#include <math.h>

class DigPow
{
  public:
    static int digPow(int, int);
    void addDigits(int);

    int count;
    int sum = 0;
};

int DigPow::digPow(int n, int p)
{
    DigPow d;
    d.count = p;
    d.addDigits(n);
    int div = d.sum / n;

    if (div * n == d.sum)
        return div;
    return -1;
}

void DigPow::addDigits(int x)
{
    if (x >= 10)
        addDigits(x / 10);

    sum += pow((x % 10), count);
    count++;
}
```

<h3>Take a Ten Minute Walk</h3>

<h4>Instructions:</h4>
You live in the city of Cartesia where all roads are laid out in a perfect grid. You arrived ten minutes too early to an appointment, so you decided to take the opportunity to go for a short walk. The city provides its citizens with a Walk Generating App on their phones -- everytime you press the button it sends you an array of one-letter strings representing directions to walk (eg. ['n', 's', 'w', 'e']). You always walk only a single block in a direction and you know it takes you one minute to traverse one city block, so create a function that will return true if the walk the app gives you will take you exactly ten minutes (you don't want to be early or late!) and will, of course, return you to your starting point. Return false otherwise.

<h4>Solution</h4>

```cpp
#include<vector>

bool isValidWalk(std::vector<char> walk) {
  if (walk.size() != 10) return false;
  int nCount = 0;
  int eCount = 0;
  for (auto i : walk)
  {
    switch (i)
    {
      case 'n':
        nCount++;
        break;
      case 'e':
        eCount++;
        break;
      case 's':
        nCount--;
        break;
      case 'w':
        eCount--;
        break;
      default:
        return false;
        break;
    }
  }   
  if ( nCount == 0 && eCount == 0) return true;
  return false;
}
```

<h3>Decode the Morse code </h3>

<h4>Instructions:</h4>
In this kata you have to write a simple Morse code decoder. While the Morse code is now mostly superceded by voice and digital data communication channels, it still has its use in some applications around the world.

The Morse code encodes every character as a sequence of "dots" and "dashes". For example, the letter `A` is coded as `·−`, letter `Q` is coded as `−−·−`, and digit `1` is coded as `·−−−−`. The Morse code is case-insensitive, traditionally capital letters are used. When the message is written in Morse code, a single space is used to separate the character codes and 3 spaces are used to separate words. For example, the message `HEY JUDE` in Morse code is `···· · −·−−   ·−−− ··− −·· ·.`

NOTE: Extra spaces before or after the code have no meaning and should be ignored.

In addition to letters, digits and some punctuation, there are some special service codes, the most notorious of those is the international distress signal SOS (that was first issued by Titanic), that is coded as `···−−−···`. These special codes are treated as single special characters, and usually are transmitted as separate words.

Your task is to implement a function that would take the morse code as input and return a decoded human-readable string.

For example:
```
decodeMorse('.... . -.--   .--- ..- -.. .')
//should return "HEY JUDE"
```

<h4>Solution</h4>

```cpp
std::string decodeMorse(std::string morseCode) {
    // ToDo: Accept dots, dashes and spaces, return human-readable message
    std::string decoded;
    std::string morseWord;
    int blankCount = 0;
    bool firstLetterScanned = false;
    
    for( auto p : morseCode ) {
      if (p == ' ')
      {
        decoded += MORSE_CODE[ morseWord ];
        morseWord = "";
        if (blankCount >= 2 && firstLetterScanned)
        {
          decoded += p;
        }
        blankCount++;
      }
      else
      {
      firstLetterScanned = true;
        blankCount = 0;
        morseWord += p;
      }
    }
    decoded += MORSE_CODE[ morseWord ];
    
    while(decoded.back() == ' ')
    {
      decoded.pop_back();
    }
    return decoded;
}
```

<h2>7 Kyu:</h2>

<h3>Isograms</h3>

<h4>Instructions:</h4>
An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram. Assume the empty string is an isogram. Ignore letter case.

```
isIsogram "Dermatoglyphics" == true
isIsogram "moose" == false
isIsogram "aba" == false
```

<h4>Solution</h4>

```cpp
#include <iostream>
bool is_isogram(std::string str) {
  std::string letters;
  for (auto c : str)
  {
    if (letters.find(std::putchar(tolower(c))) != std::string::npos) return false;   
    letters += std::putchar(tolower(c));
  }
  return true;
}5
```

<h3>Is this a triangle?</h3>

<h4>Instructions:</h4>
Implement a method that accepts 3 integer values a, b, c. The method should return true if a triangle can be built with the sides of given length and false in any other case.

(In this case, all triangles must have surface greater than 0 to be accepted).


<h4>Solution</h4>

```cpp
#include <iostream>
namespace Triangle
{
  bool isTriangle(int a, int b, int c)
  {
    int arr[] = {a, b, c};
    int n = sizeof(arr)/sizeof(arr[0]); 
    std::sort(arr, arr+n);
    long sum = (long)arr[0]+(long)arr[1];
    return sum>(long)arr[2] && arr[0] > 0;
  }
};
```

<h3>Number of People in the Bus</h3>

<h4>Instructions:</h4>
There is a bus moving in the city, and it takes and drop some people in each bus stop.

You are provided with a list (or array) of integer arrays (or tuples). Each integer array has two items which represent number of people get into bus (The first item) and number of people get off the bus (The second item) in a bus stop.

Your task is to return number of people who are still in the bus after the last bus station (after the last array). Even though it is the last bus stop, the bus is not empty and some people are still in the bus, and they are probably sleeping there :D

Take a look on the test cases.

Please keep in mind that the test cases ensure that the number of people in the bus is always >= 0. So the return integer can't be negative.

The second value in the first integer array is 0, since the bus is empty in the first bus stop.

<h4>Solution</h4>

```cpp
#include<vector>

unsigned int number(const std::vector<std::pair<int, int>>& busStops){
  unsigned int people = 0;
  for (auto &stop : busStops)
  {
    people += stop.first;
    people -= stop.second;
  }
  
  return people;
}
```

<h3>Find the next perfect square!</h3>

<h4>Instructions:</h4>
You might know some pretty large perfect squares. But what about the NEXT one?

Complete the findNextSquare method that finds the next integral perfect square after the one passed as a parameter. Recall that an integral perfect square is an integer n such that sqrt(n) is also an integer.

If the parameter is itself not a perfect square, than -1 should be returned. You may assume the parameter is positive.

Examples:
```
findNextSquare(121) --> returns 144
findNextSquare(625) --> returns 676
findNextSquare(114) --> returns -1 since 114 is not a perfect
```

<h4>Solution</h4>

```cpp
#include <cmath>    
long int findNextSquare(long int sq) {
  long int ret = sqrt(sq) + 1;
  if(sqrt(sq) != (int)sqrt(sq)){return -1;}
  return  ret*ret;
}
```